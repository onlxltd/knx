/**
 * knx.js - a KNX protocol stack in pure Javascript
 * (C) 2016-2018 Elias Karakoulakis
 */

const util = require('util');
const dgram = require('dgram');
/**
  Initializes a new KNX routing connection with provided values. Make
 sure the local system allows UDP messages to the multicast group.
**/
function IpRoutingConnection(instance) {

  instance.BindSocket = function (cb) {
    const udpSocket = dgram.createSocket({ type: 'udp4', reuseAddr: true });
    udpSocket.on('listening', () => {
      try {
        this.socket.addMembership(
          this.remoteEndpoint.addr,
          instance.localAddress
        );
      } catch (err) {
        
      }
    });
    // ROUTING multicast connections need to bind to the default port, 3671
    udpSocket.bind(3671, () => cb && cb(udpSocket));
    return udpSocket;
  };

  // <summary>
  ///     Start the connection
  /// </summary>
  instance.Connect = function () {
    this.localAddress = this.getLocalAddress();
    this.socket = this.BindSocket((socket) => {
      socket.on('error', (errmsg) => {});
      socket.on('message', (msg, rinfo, callback) => {
        this.onUdpSocketMessage(msg, rinfo, callback);
      });
      // start connection sequence
      this.transition('connecting');
    });
    return this;
  };

  return instance;
}

module.exports = IpRoutingConnection;
