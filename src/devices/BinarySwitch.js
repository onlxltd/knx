/**
 * knx.js - a pure Javascript library for KNX
 * (C) 2016 Elias Karakoulakis
 */

const Datapoint = require('../Datapoint');

class BinarySwitch {
  constructor(options, conn) {
    if (!options || !options.ga) throw 'must supply at least { ga }!';

    this.control_ga = options.ga;
    this.status_ga = options.status_ga;
    if (conn) this.bind(conn);
  }
  bind(conn) {
    this.conn = conn;
    this.control = new Datapoint({ ga: this.control_ga }, conn);
    if (this.status_ga)
      this.status = new Datapoint({ ga: this.status_ga }, conn);
  }
  // EventEmitter proxy for status ga (if its set), otherwise proxy control ga
  on(...args) {
    const tgt = this.status_ga ? this.status : this.control;
    try {
      tgt.on(...args);
    } catch (err) {
    }
  }
  switchOn() {
    this.control.write(1);
  }
  switchOff() {
    this.control.write(0);
  }
  write(v) {
    this.control.write(v);
  }
}

module.exports = BinarySwitch;
